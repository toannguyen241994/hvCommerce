﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvCommerce.Web.ViewModels
{
    public class MediaViewModel
    {
        public string Url { get; set; }

        public string ThumbnailUrl { get; set; }
    }
}
