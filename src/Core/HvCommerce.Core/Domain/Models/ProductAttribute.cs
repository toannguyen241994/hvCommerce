﻿using HvCommerce.Infrastructure.Domain.Models;

namespace HvCommerce.Core.Domain.Models
{
    public class ProductAttribute : Entity
    {
        public string Name { get; set; }
    }
}